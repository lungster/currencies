<div id="add-currency" class="modal fade">
    <div class="modal-dialog" role="dialog">
        <div class="modal-content">
            <form class="async" data-reload="true" action="/addcurrency" method="post"
                  name="createGroup" data-target="#add-group-form" autocomplete="off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Add Currency</h4>
                </div>
                <div class="modal-body">
                    <p>
                        <label>Currency name</label>
                        <input type="text" name="name" placeholder="eg. US Dollars (USD)" class="span12 form-control"
                               required=""/>
                    </p>
                    <p>
                        <label>Exchange Rate</label>
                        <input type="text" name="amount" placeholder="Exchange Rate" class="form-control"/>
                    </p>
                    <p>
                        <label>Surcharge</label>
                        <input type="text" name="surcharge" placeholder="Surcharge" class="form-control"/>
                    </p>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <input type="submit" value="Proceed" class="btn btn-primary"/>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
</div>