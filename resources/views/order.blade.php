@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <h2 class="text-center"> Buy Currencies </h2>
        <div class="card">
            <div class="card-header">Header</div>
            <div class="card-body myRow">
                <table class="table table-bordered">
                    <tr class="table-info">
                        <th>Currency</th>
                        <th>Exchange rate</th>
                        <th>Surcharge</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>{{ $currency->name }}</td>
                        <td>{{ $currency->amount }}</td>
                        <td>{{ $currency->surcharge }}</td>
                        <td>{{ $total }} for  R{{ $amount }}</td>
                        <td>
                            <form method="post" action="/createoder/{{ $currency->uuid }}">
                                <input type="text" hidden name="currency" value="{{ $total }}">
                                <input type="text" hidden name="zar" value="{{ $amount }}">
                                <input type="text" hidden name="acronym" value="{{ $currency->acronym }}">
                                <button type="submit" class="btn btn-primary btn-xs pull-right"> Proceed</button>
                                {!! csrf_field() !!}
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection