@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <h2 class="text-center"> Orders </h2>
        <div class="card">
            <div class="card-header">Orders</div>
            <div class="card-body myRow">
                <table class="table table-bordered">
                    <tr class="table-info">
                        <th>Currency Bought</th>
                        <th>Amount</th>
                        <th>Rands Paid</th>
                        <th>Date</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>{{ $order->currency->name }}</td>
                        <td>{{ $order->amount_currency }}</td>
                        <td>R{{ $order->amount_zar }}</td>
                        <td> {{ $order->created_at }}</td>
                        <td>
                            <a class="btn btn-primary btn-sm" href="/delete/{{ $order->uuid }}">Delete</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection