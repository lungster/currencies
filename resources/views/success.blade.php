@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <h2 class="text-center"> Buy Currencies </h2>
        <div class="card">
            <div class="card-header">Header</div>
            <div class="card-body myRow">
                <h2 class="alert alert-heading text-center"> Your Order has been saved! </h2>
                <div align="center">
                    <a href="/vieworders" class="btn btn-primary"> View Orders</a>
                </div>
            </div>
        </div>
    </div>
@endsection