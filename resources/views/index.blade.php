@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <h2 class="text-center"> Buy Currencies </h2>
        <div class="card">
            <div class="card-header">Header</div>
            <div class="card-body myRow">
                <table class="table table-bordered">
                    <tr class="table-info">
                        <th>Currency</th>
                        <th>Exchange rate</th>
                        <th>Surcharge</th>
                        <th>Buy</th>
                    </tr>
                    @foreach($currencies as $currency)
                        <tr>
                            <td>{{ $currency->name }}</td>
                            <td>{{ $currency->amount }}</td>
                            <td>{{ $currency->surcharge }}%</td>
                            <td>
                                <form method="post" action="/buy/{{ $currency->uuid }}">
                                    <input type="number" size="2" name="amount" placeholder="ZAR">
                                    <button type="submit" class="btn btn-primary btn-xs pull-right"> Buy</button>
                                    {!! csrf_field() !!}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    @include('add')
@endsection