<div class="alert alert-success alert-dismissible">
    <i class="icon fa fa-info"></i> <strong>Success!</strong> {{ $message }}
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
</div>