@if (Session::has('message'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ Session::get('message') }}
    </div>
@endif

@if (Session::has('status'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Success!</h4>
        {{ Session::get('status') }}
    </div>
@endif

@if (Session::has('error'))
    <div class="alert alert-error alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-info"></i> Error!</h4>
        {{ Session::get('error') }}
    </div>
@endif