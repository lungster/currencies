<div class="alert alert-warning alert-dismissible">
    <i class="icon fa fa-info"></i> <strong>Warning!</strong> {{ $message }}
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
</div>