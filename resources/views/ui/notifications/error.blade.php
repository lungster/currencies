<div class="alert alert-sticky alert-danger alert-dismissible">
    <i class="icon fa fa-info"></i> Error! {{ $message }}
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
</div>