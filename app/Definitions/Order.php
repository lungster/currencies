<?php
/**
 * Created by PhpStorm.
 * User: lungelo
 * Date: 2019/01/24
 * Time: 11:38 PM
 */

namespace App\Definitions;


use Laravel5Helpers\Definitions\Definition;

class Order extends Definition
{

    public $currency_id;

    public $user_id;

    public $amount_currency;

    public $amount_zar;

    public function __construct($currency_id, $amount, $zar)
    {
        parent::__construct([]);

        $this->currency_id = $currency_id;
        $this->user_id = '';
        $this->amount_zar = $zar;
        $this->amount_currency = $amount;

    }

    protected function setValidators()
    {
        return $this->validators = [
            'currency_id' => 'required',
            'amount_zar' => 'required',
            'amount_currency' => 'required'
        ];
    }

}