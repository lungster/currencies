<?php
/**
 * Created by PhpStorm.
 * User: lungelo
 * Date: 2019/01/24
 * Time: 11:38 PM
 */

namespace App\Definitions;


use Laravel5Helpers\Definitions\Definition;

class Currency extends Definition
{
    public $name;

    public $amount;

    public $surcharge;

    protected function setValidators()
    {
        return $this->validators = [
            'name' => 'required'
        ];
    }

}