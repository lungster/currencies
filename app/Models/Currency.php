<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class Currency extends UuidModel
{
    protected $table = 'currencies';

}
