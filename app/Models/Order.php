<?php

namespace App\Models;

use Laravel5Helpers\Uuid\UuidModel;

class Order extends UuidModel
{
    protected $table = 'orders';

    public function currency()
    {
        return $this->hasOne('App\Models\Currency', 'uuid', 'currency_id');
    }

}
