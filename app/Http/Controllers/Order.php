<?php

namespace App\Http\Controllers;

use App\Repositories\Order as Repository;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;

class Order extends Controller
{
    public function index()
    {
        try {
            $orders = $this->getRepository()->getPaginated();
            return $this->sendToView('order.index', 'View All Orders', ['orders' => $orders]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendError($exception->getMessage());
        }
    }

    public function show($id)
    {
        try {
            $order = $this->getRepository()->getResource($id);
            return $this->sendToView('order.show', 'Order Details', ['order' => $order]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendError($exception->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $this->getRepository()->deleteResource($id);

            return '<h1 class="alert-heading text-center">Order Deleted</h1><div align="center"><a class="btn btn-primary" href="/"> Go Back</a> </div>';
        } catch (LaravelHelpersExceptions $exception) {
            return $this->ajaxError($exception->getMessage());
        }
    }

    private function getRepository()
    {
        return new Repository;
    }
}
