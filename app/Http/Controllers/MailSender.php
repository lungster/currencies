<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;

class MailSender extends Controller
{
    public function send($data)
    {
        try {
            Mail::send('success', ['title' => 'Your have successfully made an order ', 'content' => $data], function ($message) {
                $message->from('admin@vetromedia.co.za', 'Lungelo Makhaya');
                $message->to('usermail@dmail.com');

            });
            return $this->sendToView('success', 'Success Page');

        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendError($exception->getMessage());
        }
    }

}
