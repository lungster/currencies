<?php

namespace App\Http\Controllers;

use App\Repositories\Currency as Repository;
use App\Definitions\Currency as Definition;
use Illuminate\Http\Request;
use App\Definitions\Order as OrderDefinition;
use App\Repositories\Order as OrderRepo;
use App\Http\Controllers\MailSender;
use Laravel5Helpers\Exceptions\LaravelHelpersExceptions;
use stdClass;

class Currency extends Controller
{
    public function index()
    {
        try {
            $data = file_get_contents('http://www.apilayer.net/api/live?access_key=69834d484506437061bd9155d8e64457');
            $json = json_decode($data);


            $currencies = $this->getRepository()->getPaginated();
            return $this->sendToView('index', 'Buy Currencies', ['currencies' => $currencies]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendError($exception->getMessage());
        }
    }

    public function store(Request $currency)
    {
        try {
            $this->getRepository()->createResource($this->getDefinition($currency));

            return 'Currency Added <a href="/"> Go Back Home</a>';
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendError($exception->getMessage());
        } catch (AmiException $exception) {
            return $this->sendError($exception->getMessage());
        }
    }

    public function show($id, Request $request)
    {
        try {
            $zar = $request->amount;
            $currency = $this->getRepository()->getResource($id);
            $total = $zar * $currency->amount;
            $surchage = ($currency->surcharge / 100) * $total;
            $totalAmont = $total - $surchage;
            return $this->sendToView('order', 'Order Details',
                ['currency' => $currency,
                    'total' => $totalAmont,
                    'amount' => $zar
                ]);
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendError($exception->getMessage());
        }
    }

    public function order($id, Request $request)
    {
        try {
            if ($request->acronym == 'GBP') {

                $data = 'You have successfully purchased ' . $request->currency . ' GBP';
                $this->sendMail()->send($data);

            } elseif ($request->acronym == 'EUR') {

                $discount = 2;
                $total = $request->zar - ($request->zar * ($discount / 100));
            }
            $this->getOrderRepo()->createResource($this->getOrderDefinition($id, $request));


            return $this->sendToView('success', 'Success Page');
        } catch (LaravelHelpersExceptions $exception) {
            return $this->sendError($exception->getMessage());
        }
    }

    private function getRepository()
    {
        return new Repository;
    }

    private function getOrderRepo()
    {
        return new OrderRepo;
    }

    private function getDefinition(Request $request)
    {
        return new Definition($request->all());
    }

    private function getOrderDefinition($id, Request $request)
    {
        $amount = $request->currency;
        $zar = $request->zar;
        return new OrderDefinition($id, $amount, $zar);
    }

    private function sendMail()
    {
        return new MailSender;
    }

}
