<?php

namespace App\Repositories;

use Laravel5Helpers\Repositories\Repository;
use App\Models\Order as Model;

class Order extends Repository
{

    protected function getModel()
    {
        return new Model;
    }
}
