<?php

namespace App\Repositories;

use Illuminate\Database\QueryException;
use Laravel5Helpers\Repositories\Repository;
use App\Models\Currency as Model;

class Currency extends Repository
{
    public function getAll()
    {
        try {
            return $this->getModel()->where('active', 1)->get();
        } catch (QueryException $exception) {
            throw new ResourceGetError($this->getModelShortName());
        } catch (\PDOException $exception) {
            throw new ResourceGetError($this->getModelShortName());
        }
    }

    protected function getModel()
    {
        return new Model;
    }
}
