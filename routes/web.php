<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'Currency@index')->name('home');
Route::post('/buy/{id}', 'Currency@show');
Route::post('/createoder/{id}', 'Currency@order');
Route::get('/vieworders', 'Order@index');
Route::get('/vieworder/{id}', 'Order@show');
Route::get('/delete/{id}', 'Order@destroy');
Route::post('/addcurrency', 'Currency@store')->name('home');
